import EventEmitter from "eventemitter3";
import image from "../images/planet.svg";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  _loading = null;

  constructor() {
    super();

    this._loading = document.querySelector('progress.progress');
    this._loading.style.display = 'none';

    this._startLoading();

    this.emit(Application.events.READY);
  }

  _create(planet) {
    const box = document.createElement("div");
    box.classList.add("box");
    box.innerHTML = this._render(planet);

    document.body.querySelector(".main").appendChild(box);
  }

  async _load(url = null) {
    await fetch(url || 'https://swapi.boom.dev/api/planets')
      .then((response) => response.json())
      .then((data) => {
        data.results.forEach((planet) => {
          this._create(planet);
        });

        if(data.next !== null) {
          this._load(data.next);
        } else {
          // resolve();
        }
      });
  }

  _startLoading() {
    this._loading.style.display = 'block';

    return new Promise((resolve, reject) => {
      this._load().then(() => {
        this._stopLoading();
        resolve();
      });
    });
  }

  _stopLoading() {
    this._loading.style.display = 'none';
    console.log('done!');
  }

  _render({ name, terrain, population }) {
    return `
<article class="media">
  <div class="media-left">
    <figure class="image is-64x64">
      <img src="${image}" alt="planet">
    </figure>
  </div>
  <div class="media-content">
    <div class="content">
    <h4>${name}</h4>
      <p>
        <span class="tag">${terrain}</span> <span class="tag">${population}</span>
        <br>
      </p>
    </div>
  </div>
</article>
    `;
  }
}
